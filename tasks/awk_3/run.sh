#!/bin/bash
awk '{sum = ($2 + $3 + $4)
      if (sum >= 80 * 3) {print $0 " : A"}
      else if (sum >= 60 * 3) {print $0 " : B"}
      else if (sum >= 50 * 3) {print $0 " : C"}
      else {print $0 " : FAIL"}}'
