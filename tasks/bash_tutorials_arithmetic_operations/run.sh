#!/bin/bash
read e
res=$(echo "scale=20; $e" | bc)
echo $res | awk '{printf "%.3f", $0}'
