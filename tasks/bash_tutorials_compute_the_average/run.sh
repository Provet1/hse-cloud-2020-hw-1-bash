#!/bin/bash
read n
awk 'BEGIN{sum=0; cnt=0}{sum=sum+$1; cnt=cnt+1}END{printf "%.3f", sum/cnt}'
