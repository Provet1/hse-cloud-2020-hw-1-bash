#!/bin/bash
declare -a arr;
while read line
do
    if [[ "$line" != *"a"* && "$line" != *"A"* ]]; then 
        arr=("${arr[@]}" $line) 
    fi
done
echo ${arr[@]}
