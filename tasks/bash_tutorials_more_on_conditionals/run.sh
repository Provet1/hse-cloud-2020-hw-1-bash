#!/bin/bash
read x
read y
read z
if [[ $x == $y && $y == $z ]]
then
    echo -e "EQUILATERAL\c"
elif [[ $x == $y || $x == $z || $y == $z ]]
then
    echo -e "ISOSCELES\c"
else
    echo -e "SCALENE\c"
fi
