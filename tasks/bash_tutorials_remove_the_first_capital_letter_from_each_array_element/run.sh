#!/bin/bash
declare -a arr;
while read line
do
    arr=("${arr[@]}" $line) 
done
echo ${arr[@]/[A-Z]/.}
